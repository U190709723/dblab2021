CALL selectAllCustomers();

CALL getCustomersByCountry('Spain');

SET @country = "UK";
SELECT @country;

CALL getCustomersByCountry(@country);

SELECT @country;

SELECT * FROM shippers;

SELECT COUNT(orderID) AS 'No. of orders' FROM orders 
JOIN shippers ON orders.shipperID = shippers.shipperID
WHERE shippername="Speedy Express";

SET @orderCount = 0;
CALL getNumberOfOrdersByShipper('Speedy Express', @orderCount);
SELECT @orderCount;


set @beg = 100;
set @inc = 10;
CALL counter(@beg, @inc);
SELECT @beg;
SELECT @inc;


USE movie_db;

SELECT * FROM movies;
SELECT * FROM denormalized;


LOAD DATA INFILE "put location of the CSV file here" 
INTO TABLE denormalized
COLUMNS TERMINATED BY ';';

show variables like "secure_file_priv"


 


