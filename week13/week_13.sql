
#Using temprorary tables for storing results temporarly

USE company_products;

DROP TEMPORARY TABLE IF EXISTS
products_below_average;

CREATE TEMPORARY TABLE products_below_average
SELECT products.id, products.name, products.price
FROM products
WHERE price < (SELECT AVG(price) FROM products);

SHOW TABLES;

DROP TABLE products_below_average







