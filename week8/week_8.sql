USE company;

# 1:
SELECT ShipperID, OrderID 
FROM orders
WHERE ShipperID IN (
	SELECT ShipperID FROM shippers
);


# 2:
SELECT shipperid, COUNT(OrderId) AS 'No. of orders'
FROM orders
WHERE ShipperID IN (
	SELECT ShipperID FROM shippers
)
GROUP BY ShipperID;

# OR
SELECT ShipperName, COUNT(OrderId) AS 'No. of orders'
FROM orders JOIN shippers ON orders.ShipperID = shippers.ShipperID GROUP BY orders.ShipperID;


# 3. Find customer names who have made at least one order at our company
SELECT CustomerName, OrderID 
FROM orders
JOIN customers on customers.CustomerID = orders.CustomerID
ORDER BY OrderID;

# 3a. All customer names, whether or not they made any orders
SELECT CustomerName, OrderID 
FROM customers
LEFT JOIN orders on customers.CustomerID = orders.CustomerID
ORDER BY OrderID;


# 4 Find all the employee names that prepared an order
SELECT FirstName, LastName, OrderID
FROM orders 
JOIN employees on orders.EmployeeID = employees.EmployeeID
ORDER BY OrderID;


# 4a Find all the employee names that prepared an order 
# and list the order ids that they worked on if they prepared any orders
SELECT FirstName, LastName, OrderID
FROM orders 
RIGHT JOIN employees on orders.EmployeeID = employees.EmployeeID
ORDER BY OrderID;










